from django.urls import path, include
from . import views

urlpatterns = [
    path("", views.home, name="home"),
    path("login", views.login_view, name="login"),
    path("logout", views.logout_view, name="logout"),
    path("student_index", views.student_index, name="student_index"),
    path("faculty_index", views.faculty_index, name="faculty_index"),
    path("attendance/<int:ClassSubject_id>/<str:lecture_date>", views.attendance, name="attendance"),
    path("register_attendance/<int:ClassSubject_id>/<str:Date>", views.register_attendance, name="register_attendance"),
]
