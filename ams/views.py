from decimal import DivisionByZero
from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse, HttpResponseRedirect
from .models import ClassSubject, Class, Student, Lecture, Attendance
from django.urls import reverse
from datetime import datetime

# Create your views here.

def home(request):
    if request.method == "GET":
        return render(request, "ams/home.html")
        
def login_view(request):
    if request.method == "GET":
        return render(request, "ams/login.html", {"message":None})
    else:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            if user.is_superuser:
                return HttpResponseRedirect("admin/")
            else:
                try:
                    request.user.student
                    return HttpResponseRedirect(reverse("student_index"))
                except Exception:
                    return HttpResponseRedirect(reverse("faculty_index"))
        else:
            return render(request, "ams/login.html", {"message":"Invalid credentials."})

        
def student_index(request):
    if not request.user.is_authenticated:
        return render(request, "ams/login.html", {"message":None})
   
    class_subject = ClassSubject.objects.filter(Class=request.user.student.Class)
    
    sub_attendance = dict()

    for i in class_subject:
        total_attendance = i.subject.lecture_set.filter(Class = request.user.student.Class).count()
        student_attendance = request.user.student.attendance_set.filter(status='P', lecture__subject=i.subject).count()

        try:
            percentage = round(student_attendance / total_attendance * 100)
        except ZeroDivisionError:
            percentage = 0

        sub_attendance[i.subject.name] = percentage
    
    context = {
        "class_subject" : class_subject,
        "sub_attendance" : sub_attendance,
    }
    return render(request, "ams/student_index.html", context)

def faculty_index(request):
    if not request.user.is_authenticated:
        return render(request, "ams/login.html", {"message":None})
    
    context = {
        "faculty_subjects" : ClassSubject.objects.filter(faculty=request.user.faculty)
    }
    return render(request, "ams/faculty_index.html", context)

def attendance(request, ClassSubject_id, lecture_date):
    
    c = ClassSubject.objects.get(pk=ClassSubject_id)

    context = {
        "ClassSubject_id" : ClassSubject_id,
        "Class" : Class.objects.get(id=c.Class.id),
        "students" : Student.objects.filter(Class=c.Class.id),
        "Date" : lecture_date,
    }

    return render(request, "ams/attendance.html", context)

def register_attendance(request, ClassSubject_id, Date):
    c = ClassSubject.objects.get(pk=ClassSubject_id)
    d = datetime.strptime(Date, '%d-%m-%Y').date()

    if not Lecture.objects.filter(Date=d, Class=c.Class, subject=c.subject, faculty=c.faculty).exists():
        l = Lecture(Date=d, Class=c.Class, subject=c.subject, faculty=c.faculty)
        l.save()
    else:
        return HttpResponse("Already marked!!")
    students = c.Class.student_set.all()

    for student in students:        
        answer = request.POST.dict()
        a = Attendance(student=student, lecture=l , status=answer[student.enrollment_number])
        a.save()
    return HttpResponseRedirect(reverse("faculty_index"))

def logout_view(request):
    logout(request)
    return render(request, "ams/home.html")