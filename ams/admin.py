from django.contrib import admin


from .models import Department, Student, Faculty, Subject, Class, ClassSubject, Lecture, Attendance
# Register your models here.

admin.site.register(Department)
admin.site.register(Student)
admin.site.register(Faculty)
admin.site.register(Subject)
admin.site.register(Class)
admin.site.register(ClassSubject)
admin.site.register(Lecture)
admin.site.register(Attendance)
