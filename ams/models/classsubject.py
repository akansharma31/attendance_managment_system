from django.db import models

from . import Subject, Faculty, Class

class ClassSubject(models.Model):
    id = models.AutoField(primary_key=True)
    Class = models.ForeignKey(Class, on_delete=models.CASCADE, null=True)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE, null=True)
    faculty = models.ForeignKey(Faculty, on_delete=models.CASCADE, null=True)
