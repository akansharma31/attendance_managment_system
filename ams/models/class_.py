from django.db import models
from . import Department, Faculty

SEMESTER_CHOICES = (
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
    ('4', '4'),
    ('5', '5'),
    ('6', '6'),
    ('7', '7'),
    ('8', '8'),
)
SECTIONS = (
    ('A', 'A'),
    ('B', 'B'),
    ('C', 'C'),
    ('D', 'D'),
)
class Class(models.Model):
    id = models.AutoField(primary_key=True)
    section = models.CharField(max_length=1, choices=SECTIONS)
    batch = models.CharField(max_length=4)
    current_semester = models.CharField(max_length=1, choices=SEMESTER_CHOICES)
    department = models.ForeignKey(Department, on_delete=models.CASCADE)
    coordinator = models.OneToOneField(Faculty, on_delete=models.CASCADE)

    def __str__(self):
        return self.department.name + "-" + self.section
