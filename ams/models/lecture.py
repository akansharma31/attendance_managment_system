from django.db import models

from . import Subject, Faculty, Class

class Lecture(models.Model):
    id = models.AutoField(primary_key=True)
    Date = models.DateField()
    Class = models.ForeignKey(Class, on_delete=models.CASCADE, null=True)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE, null=True)
    faculty = models.ForeignKey(Faculty, on_delete=models.CASCADE, null=True)

    