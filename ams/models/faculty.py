from tkinter import CASCADE
from django.db import models
from . import Department
from django.contrib.auth.models import User


class Faculty(models.Model):
    department = models.ForeignKey(Department, on_delete=models.CASCADE)
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.user.first_name + " "  + self.user.last_name