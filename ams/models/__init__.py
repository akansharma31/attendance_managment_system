from .department import Department
from .faculty import Faculty
from .class_ import Class
from .student import Student
from .subject import Subject
from .classsubject import ClassSubject
from .lecture import Lecture
from .attendance import Attendance