from tkinter import CASCADE
from django.db import models
from . import Class
from django.contrib.auth.models import User

class Student(models.Model):
    SEMESTER_CHOICES = (
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
        ('6', '6'),
        ('7', '7'),
        ('8', '8'),
    )
    enrollment_number = models.CharField(max_length=15)
    semester = models.CharField(max_length=1, choices=SEMESTER_CHOICES)
    admission_year = models.CharField(max_length=4)
    Class = models.ForeignKey(Class, on_delete=models.CASCADE, null=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.user.first_name + " "  + self.user.last_name
        