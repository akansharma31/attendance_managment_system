from django.db import models

from . import Student, Lecture

CHOICES = (
    ('P', 'Present'),
    ('A', 'Absent'),
)

class Attendance(models.Model):
    id = models.AutoField(primary_key=True)
    student = models.ForeignKey(Student, on_delete=models.CASCADE, null=True)
    lecture = models.ForeignKey(Lecture, on_delete=models.CASCADE, null=True)
    status = models.CharField(max_length=1, choices=CHOICES)
